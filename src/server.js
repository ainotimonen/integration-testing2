const express = require('express');
const converter = require('./converter');
const app = express();
const PORT = 3000;

// Welcomeendpoint
app.get('/', (req, res) => res.send("Welcome!"));

//RGB to Hex endpoint
app.get('/rgb-to-hex', (req, res) => {
    const red = req.query.r;
    const green = req.query.g;
    const blue = req.query.b;
    const hex = converter.rgbToHex(red, green, blue);
    res.send(hex);
});

console.log("NODE_ENV: " + process.env.NODE_ENV);
if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(PORT, () => console.log("Server listening..."));
}



